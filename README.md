# JWT SPOA

**This project is archived!**

Please see [https://gitlab.com/andrewheberle/jwt-verify](https://gitlab.com/andrewheberle/jwt-verify).

All future development will occur on JWT Verify only.

This is a Stream Processing Offload Agent (SPOA) for [HAProxy](https://www.haproxy.org) to
validate a request has a valid JSON Web Token (JWT) provided.

In its current form, this SPOA is specifically targeted at verifiying JWT's
issued by Cloudflare Access to secure on-premise web applications.

Documentation for Cloudflare Access and the use of JSON Web Tokens is here:

[https://developers.cloudflare.com/access/advanced-management/validating-json](https://developers.cloudflare.com/access/advanced-management/validating-json)

Full documentation on this HAProxy feature is here:

[https://www.haproxy.org/download/2.2/doc/SPOE.txt](https://www.haproxy.org/download/2.2/doc/SPOE.txt)

## Usage

### Command line flags

```sh
jwt-spoe -help
Usage of jwt-spoa:
    --address string         listen address (default "127.0.0.1")
    --auth-domain string     authentication domain (.cloudflareaccess.com is appended)
    --port int               listen port (default 9000)
    --spoe-response string   spoe response variable name (default "valid")
```

* `address`: This is the address for the SPOA to listen on. This should NOT be publically accessible.
* `auth-domain`: The Cloudflare Access authentication domain (only the part before `.cloudflareaccess.com` should be entered).
* `port`: The TCP port to listen on.
* `spoe-reponse`: The variable name to set for a JWT that passes signature verification. This is a transaction (txn) level variable.

### Environment Variables

All configuration options that can be specified on the command line can be provided by environment variables:

* `JWT_ADDRESS`: listen address
* `JWT_AUTH-DOMAIN`: authentication domain
* `JWT_PORT`: listen port
* `JWT_SPOE-RESPONSE`: spoe response variable name

### Configuration

Configure HAProxy:

```haproxy
# example HTTPS listener
listen https
    bind :443 ssl crt /etc/haproxy/server.pem alpn h2,http/1.1
    mode http

    balance roundrobin
    timeout connect 5s
    timeout client  30s
    timeout server  30s

    # load spoe config
    filter spoe engine jwt-verify config /etc/haproxy/spoe.cfg

    # set audience value for application and send to spoe-group
    http-request set-var(txn.aud) str("audience value for service")
    http-request send-spoe-group jwt-verify jwt-verify

    # only allow requests with a valid jwt by checking the variable set
    http-request deny if ! { var(txn.jwt_verify.valid) -m bool }

    server server1 <IP ADDRESS>:<PORT> ssl check

# our SPOA
backend spoe
    mode tcp

    balance roundrobin
    timeout connect 5s
    timeout server  3m

    server jwt-spoa 127.0.0.1:9000
```

Create `/etc/haproxy/spoe.cfg`:

```haproxy
[jwt-verify]
spoe-agent jwt-verify
    log         global
    option      var-prefix jwt_verify

    timeout     hello      2s
    timeout     processing 10ms
    timeout     idle       2m

    groups      jwt-verify

    use-backend spoe

spoe-message jwt-verify
    args        jwt=req.hdr(cf-access-jwt-assertion)
    args        aud=var(txn.aud)

spoe-group jwt-verify
    messages    jwt-verify
```

Start SPOA:

```sh
jwt-spoa -auth-domain <ACCESS AUTH DOMAIN>
```

Reload HAProxy configuration:

```sh
systemctl reload haproxy
```

## Running as a service

An example systemd unit file is can be built and used as follows:

```sh
make -C systemd jwt-spoa@.service
cp systemd/jwt-spoa@.service /etc/systemd/system
useradd -d / -M -s /sbin/nologin jwt-spoa
systemctl daemon-reload
systemctl enable jwt-spoa@<ACCESS AUTH DOMAIN>.service
systemctl start jwt-spoa@<ACCESS AUTH DOMAIN>.service
```

## Limitations

The SPOA does nothing with any claims within the JWT, it only verifies the
signature is valid.

As noted above, this is currently very Cloudflare Access centric.
