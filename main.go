package main

import (
	"context"
	"fmt"

	"github.com/coreos/go-oidc"
	spoe "github.com/criteo/haproxy-spoe-go"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	ctx    = context.TODO()
	keySet oidc.KeySet
)

func jwtValid(jwt, aud string) (bool, error) {
	// verify jwt
	verifier := oidc.NewVerifier(fmt.Sprintf("https://%s.cloudflareaccess.com", viper.GetString("auth-domain")), keySet, &oidc.Config{ClientID: aud})
	_, err := verifier.Verify(ctx, jwt)

	if err != nil {
		return false, err
	}

	return true, nil
}

// jwtValidate function
func jwtValidate(messages *spoe.MessageIterator) ([]spoe.Action, error) {
	var accessJWT, policyAUD string
	var uuid = uuid.New().String()

	for messages.Next() {
		msg := messages.Message

		if msg.Name != "jwt-verify" {
			continue
		}

		// logging
		log.Info().Str("uuid", uuid).Msg("handling message")

		// loop through args
		for k, v := range msg.Args.Map() {
			var ok bool

			switch k {
			case "jwt":
				accessJWT, ok = v.(string)
				if !ok {
					return nil, fmt.Errorf("jwt value was not a string")
				}
			case "aud":
				policyAUD, ok = v.(string)
				if !ok {
					return nil, fmt.Errorf("aud value was not a string")
				}
			}
		}

		// check we got the data required
		if accessJWT == "" || policyAUD == "" {
			return nil, fmt.Errorf("missing jwt or aud")
		}

		// validate jwt
		if ok, err := jwtValid(accessJWT, policyAUD); !ok {
			return nil, fmt.Errorf("jwt verification failed: %w", err)
		}
	}

	// if we get here the jwt was valid
	return []spoe.Action{
		spoe.ActionSetVar{
			Name:  viper.GetString("spoe-response"),
			Scope: spoe.VarScopeTransaction,
			Value: true,
		},
	}, nil
}

func main() {
	// command line args
	pflag.String("address", "127.0.0.1", "listen address")
	pflag.Int("port", 9000, "listen port")
	pflag.String("auth-domain", "", "authentication domain (.cloudflareaccess.com is appended)")
	pflag.String("spoe-response", "valid", "spoe response variable name")

	// parse flags and bind to viper
	pflag.Parse()
	_ = viper.BindPFlags(pflag.CommandLine)

	// look at the environment also
	viper.SetEnvPrefix("jwt")
	viper.AutomaticEnv()

	// check auth-domain was provided
	if viper.GetString("auth-domain") == "" {
		log.Fatal().Msg("auth-domain cannot be empty")
	}

	keySet = oidc.NewRemoteKeySet(ctx, fmt.Sprintf("https://%s.cloudflareaccess.com/cdn-cgi/access/certs", viper.GetString("auth-domain")))

	// start spoa
	agent := spoe.New(jwtValidate)
	if err := agent.ListenAndServe(fmt.Sprintf("%s:%d", viper.GetString("address"), viper.GetInt("port"))); err != nil {
		log.Err(err).Msg("SPOA error")
	}
}
