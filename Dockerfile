FROM golang:1.15-alpine as builder

RUN apk --no-cache add git && \
    env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go get -u gitlab.com/andrewheberle/jwt-spoa

FROM scratch

COPY --from=builder /go/bin/jwt-spoa /jwt-spoa

ENTRYPOINT [ "/jwt-spoa" ]
