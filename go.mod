module gitlab.com/andrewheberle/jwt-spoa

go 1.13

require (
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/criteo/haproxy-spoe-go v1.0.1
	github.com/google/uuid v1.1.2
	github.com/pquerna/cachecontrol v0.0.0-20200921180117-858c6e7e6b7e // indirect
	github.com/rs/zerolog v1.20.0
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.7.1
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
